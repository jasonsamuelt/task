
const shell = require('shelljs');

shell.mkdir("dist/models/migration_seeds/")
shell.cp('-R', 'src/models/migration_seeds/*.json', 'dist/models/migration_seeds/');
shell.cp('.env', 'dist/.env');
console.log("Coping Static Asserts");