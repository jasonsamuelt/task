export const routesPath = {
  // Seeds
  GET_DEPARTMENT: "/departments",
  GET_PROJECT: "/projects",

  // Users
  GET_EMPLOYEES: "/employees",
  USER_DEPARTMENT: "/userDepartment",
  USER_PROJECT: "/userProject",
  GET_USER_DEPARTMENT: "/userDepartments",
  GET_USER_PROJECTS: "/userProjects",
};
