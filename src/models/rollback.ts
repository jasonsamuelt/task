import * as Sequelize from "./base";
import { rollback } from "./migration";

Sequelize.initialize()
  .then((sequelize) => rollback(sequelize))
  .catch((err) => {
    console.log(err);
    console.log("Mysql connection error. Please make sure Mysql is running.");
    process.exit();
  });
