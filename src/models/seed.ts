import * as Sequelize from "./base";

export async function runSeedFiles() {
  try {
    const seedFiles: any = [
      { model: "Department", file: "departments.json" },
      { model: "Project", file: "projects.json" },
      { model: "Role", file: "roles.json" },
      { model: "User", file: "users.json" },
      { model: "UserProject", file: "userProjects.json" },
      { model: "UserDepartment", file: "userDepartments.json" },
    ];
    for (const seedFile of seedFiles) {
      console.log(`Running Seed File ${seedFile.file}`);
      const datas = require(`./migration_seeds/${seedFile.file}`);
      const sequelize = await Sequelize.getInstance();
      const seedModel = await sequelize.model(seedFile.model);
      for (const data of datas) {
        await seedModel.upsert(data);
      }
    }
  } catch (error) {
    throw error;
  }
}
