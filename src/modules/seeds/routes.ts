import { Express } from "express";
import { authenticate, handle } from "../common/request-handler";
import { routesPath } from "../../constants/routes";
import * as controller from "./controller";

export default function (app: Express) {
  app.get(routesPath.GET_DEPARTMENT, handle(controller.getDepartments));
  app.get(routesPath.GET_PROJECT, handle(controller.getProjects));
}
