"use strict";
import { RequestHandler } from "../common/request-handler";
import * as Service from "./service";

export const getDepartments = async (handler: RequestHandler) => {
  try {
    const res: any = await Service.getDepartments();
    return handler.handleResponse(res);
  } catch (err) {
    return handler.sendServerError(err);
  }
};
export const getProjects = async (handler: RequestHandler) => {
  try {
    const res: any = await Service.getProjects();
    return handler.handleResponse(res);
  } catch (err) {
    return handler.sendServerError(err);
  }
};
