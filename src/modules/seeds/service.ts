"use strict";
import * as SeedsRepo from "./repo";
import * as _ from "lodash";

export const getDepartments = () => SeedsRepo.getDepartments();
export const getProjects = () => SeedsRepo.getProjects();
