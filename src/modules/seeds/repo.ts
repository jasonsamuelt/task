"use strict";
import Department from "./models/Department";
import Project from "./models/Project";

export const getDepartments = async () => await Department.findAll();
export const getProjects = async () => await Project.findAll();
