import {
  AutoIncrement,
  BelongsTo,
  Column,
  DataType,
  ForeignKey,
  Model,
  PrimaryKey,
  Table,
} from "sequelize-typescript";
import Department from "./Department";

@Table({
  tableName: "project",
})
export default class Project extends Model<Project> {
  @PrimaryKey
  @AutoIncrement
  @Column({
    type: DataType.INTEGER,
  })
  id: number;

  @ForeignKey(() => Department)
  @Column({ type: DataType.INTEGER, allowNull: true })
  departmentId: number;

  @Column({ type: DataType.STRING, allowNull: true })
  name: string;

  @BelongsTo(() => Department)
  department: Department;
}
