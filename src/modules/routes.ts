import { Express } from "express";
import { default as seedRoutes } from "./seeds/routes";
import { default as userRoutes } from "./users/routes";

export default function (app: Express) {
  app.get("/", (req, res) =>
    res.status(200).send({ message: "Task app has been initiated" })
  );
  seedRoutes(app);
  userRoutes(app);
}
