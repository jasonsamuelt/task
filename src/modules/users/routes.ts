import { Express } from "express";
import { authenticate, handle } from "../common/request-handler";
import { routesPath } from "../../constants/routes";
import * as controller from "./controller";

export default function (app: Express) {
  app.put(routesPath.USER_DEPARTMENT, handle(controller.userDepartments));
  app.put(routesPath.USER_PROJECT, handle(controller.userProjects));
  app.get(
    routesPath.GET_USER_DEPARTMENT,
    handle(controller.getUserDepartments)
  );
  app.get(routesPath.GET_USER_PROJECTS, handle(controller.getUserProjects));
  app.get(routesPath.GET_EMPLOYEES, handle(controller.getEmployees));
  // app.get(routesPath.USER_PROJECT, handle(controller.getuserDepartments));
}
