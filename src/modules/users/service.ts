"use strict";
import * as Users from "./repo";
import * as _ from "lodash";

export const userDepartments = (data: any) => Users.userDepartments(data);
export const userProjects = (data: any) => Users.userProjects(data);
export const getUserDepartments = () => Users.getUserDepartments();
export const getUserProjects = () => Users.getUserProjects();
export const getEmployees = () => Users.getEmployees();
