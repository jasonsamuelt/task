"use strict";
import Department from "../seeds/models/Department";
import Project from "../seeds/models/Project";
import User from "./models/User";
import UserDepartment from "./models/UserDepartment";
import UserProject from "./models/UserProject";
import UserDepartmentProject from "./models/UserProject";

export const userDepartments = async (data: any) =>
  await UserDepartment.findAll();
export const userProjects = async (data: any) => await UserProject.findAll();
export const getUserDepartments = async () =>
  await UserDepartment.findAll({ include: [User, Department] });
export const getUserProjects = async () =>
  await UserProject.findAll({ include: [User, Project] });
export const getEmployees = async () =>
  await User.findAll({ where: { roleId: 2 } });
