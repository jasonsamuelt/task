"use strict";
import { RequestHandler } from "../common/request-handler";
import * as Service from "./service";

export const userDepartments = async (handler: RequestHandler) => {
  try {
    const data = handler.getBody();
    const res: any = await Service.userDepartments(data);
    return handler.handleResponse(res);
  } catch (err) {
    return handler.sendServerError(err);
  }
};
export const userProjects = async (handler: RequestHandler) => {
  try {
    const data = handler.getBody();
    const res: any = await Service.userProjects(data);
    return handler.handleResponse(res);
  } catch (err) {
    return handler.sendServerError(err);
  }
};
export const getUserDepartments = async (handler: RequestHandler) => {
  try {
    const res: any = await Service.getUserDepartments();
    return handler.handleResponse(res);
  } catch (err) {
    return handler.sendServerError(err);
  }
};
export const getUserProjects = async (handler: RequestHandler) => {
  try {
    const res: any = await Service.getUserProjects();
    return handler.handleResponse(res);
  } catch (err) {
    return handler.sendServerError(err);
  }
};
export const getEmployees = async (handler: RequestHandler) => {
  try {
    const res: any = await Service.getEmployees();
    return handler.handleResponse(res);
  } catch (err) {
    return handler.sendServerError(err);
  }
};
