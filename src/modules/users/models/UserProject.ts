import {
  AutoIncrement,
  BelongsTo,
  Column,
  DataType,
  ForeignKey,
  Model,
  PrimaryKey,
  Table,
} from "sequelize-typescript";
import Project from "../../seeds/models/Project";
import User from "./User";

@Table({
  tableName: "user_project",
})
export default class UserProject extends Model<UserProject> {
  @PrimaryKey
  @AutoIncrement
  @Column({
    type: DataType.INTEGER,
  })
  id: number;

  @ForeignKey(() => User)
  @Column({ type: DataType.INTEGER, allowNull: true })
  userId: number;

  @ForeignKey(() => Project)
  @Column({ type: DataType.INTEGER, allowNull: true })
  projectId: number;

  @BelongsTo(() => User)
  user: User;

  @BelongsTo(() => Project)
  project: Project;
}
