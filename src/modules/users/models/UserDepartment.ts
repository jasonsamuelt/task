import {
  AutoIncrement,
  BelongsTo,
  Column,
  DataType,
  ForeignKey,
  Model,
  PrimaryKey,
  Table,
} from "sequelize-typescript";
import Department from "../../seeds/models/Department";
import User from "./User";

@Table({
  tableName: "user_department",
})
export default class UserDepartment extends Model<UserDepartment> {
  @PrimaryKey
  @AutoIncrement
  @Column({
    type: DataType.INTEGER,
  })
  id: number;

  @ForeignKey(() => User)
  @Column({ type: DataType.INTEGER, allowNull: true })
  userId: number;

  @ForeignKey(() => Department)
  @Column({ type: DataType.INTEGER, allowNull: true })
  departmentId: number;

  @BelongsTo(() => User)
  user: User;

  @BelongsTo(() => Department)
  department: Department;
}
